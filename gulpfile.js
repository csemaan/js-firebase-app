var gulp = require('gulp');

var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css');
var minifyHTML = require('gulp-minify-html');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync'); // Reload the browser on file changes

gulp.task('css-sass-min', function() {
  gulp.src('./dev/assets/css/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(browserSync.reload({
      stream: true,
    }));
});

gulp.task('static-assets', function() {
  gulp.src('./dev/*')
    .pipe(gulp.dest('./public/'));
});

gulp.task('js-min', function() {
  gulp.src('./dev/assets/js/main.js')
    .pipe(uglify().on('error', function(e){
            console.log(e);
         }))
    .pipe(gulp.dest('./public/assets/js/'));
});

gulp.task('img-min', function() {
  gulp.src('./dev/assets/img/**/*')
  .pipe(imagemin({
    progressive: true,
  }))
  .pipe(gulp.dest('./public/assets/img'));
});

// Serve application
gulp.task('serve', ['css-sass-min', 'static-assets', 'js-min', 'img-min'], function() {
  browserSync.init({
    server: {
      baseDir: 'public',
    },
  });
});

// Run all Gulp tasks and serve application
gulp.task('default', ['serve'], function() {
  gulp.watch('dev/assets/css/*.scss', ['css-sass-min']);
  gulp.watch('dev/*', ['static-assets']);
  gulp.watch('dev/assets/js/*.js', ['js-min']);
  gulp.watch('dev/*.html', browserSync.reload);
  gulp.watch('dev/assets/js/**/*.js', browserSync.reload);
});