<?php 
  header("Access-Control-Allow-Origin: *");
  if(isset($_POST) && isset($_GET["func"])) {
    switch ($_GET["func"]) {
      case 'resizeImage':
        echo resizeImage($_POST);
        break;
      
      case 'getDocTitle':
        echo getDocTitle($_POST);
        break;
      
      default:
        break;
    }
  }
  else {
    echo 0;
  }

  function getDocTitle() {
    $title = '';
    $dom = new DOMDocument();
    if($dom->loadHTMLFile($_POST["input-link-url"])) {
        $list = $dom->getElementsByTagName("title");
        if ($list->length > 0) {
            $title = $list->item(0)->textContent;
        }
    }
    return $title;
  }
  
  function resizeImage($data) {
	require("SimpleImage.php");
    try {
      $file = $data["input-binder-cover"];
      $image = new SimpleImage();
      $image -> fromFile($file) -> autoOrient();
      $type = $image -> getMimeType();
      return json_encode(array(
        "imgData" => $image -> thumbnail(300, 200, "center") -> toDataUri($type, 70), 
        "imgType" => $type,
		    "extra"	=> "Call me Ishmael"
      ));    
    }
    catch(Exception $err) {
		// TODO : remove
		echo $err;
		return 0;
    }
  }