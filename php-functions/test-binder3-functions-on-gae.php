<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Test / Binders3Functions</title>
  </head>
  <body>
	<h1>Test Binders3 Functions on GAE</h1>
    <h2>Resize Image</h2>
    <form method="post" action="https://binder3-functions.appspot.com/binder3-functions.php?func=resizeImage">
      Image URL : <input type="text" name="input-binder-cover">
      <input type="submit" value="Resize Image">
    </form>
    <h2>Get Document Title</h2>
    <form method="post" action="https://binder3-functions.appspot.com/binder3-functions.php?func=getDocTitle">
      Document URL : <input type="text" name="input-link-url">
      <input type="submit" value="Get Document Title">
    </form>
  </body>
</html>