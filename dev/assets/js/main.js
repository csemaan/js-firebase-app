!(function() {

/***************************************
Global vars 
****************************************/

  // External PHP services.
  var phpServerAddress = "https://binder3-functions.appspot.com/";
  // Firebase Database Service Reference.
  var fbDatabase = firebase.database();
  // Global variables used for logged-in user
  var fbuDbRoot = null; // binder3-v2/users/[user-id] 
  var fbuBinders = null; // binder3-v2/users/[user-id]/binders 
  var fbuCurrentBinder = null; // binder3-v2/users/[user-id]/binders/[binder-id] 
  // Sorting globals
  var sortedBinders = null;
  var sortedLinks = null;

  // Tested and working, but not used in current iteration as Database suffices 
  // for storing small image files. Could be used if larger files need to be stored.
  /*
  var fbStorage = firebase.storage();
  var fbuStore = null;
  */

  // FirebaseUI config.
  var uiConfig = {
    'callbacks': {
      // Called when the user has been successfully signed in.
      'signInSuccess': function(user, credential, redirectUrl) {
        handleSignedInUser(user);
        // Do not redirect.
        return false;
      }
    },
    //signInSuccessUrl: '',
    signInOptions: [
      // Only Google and Email accounts Providers.
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      //firebase.auth.TwitterAuthProvider.PROVIDER_ID,
      //firebase.auth.GithubAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    signInFlow: "popup",
    // Terms of service url.
    tosUrl: 'index.html'
  };

  // Initialize the FirebaseUI Widget using Firebase.
  var ui = new firebaseui.auth.AuthUI(firebase.auth());
  var currentUid = null;

  // Detect iOS for disabling Sortable functions
  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  // Dropzone mouseover state
  var dropzoneMouseOverState = false;

/***************************************
END - Global vars 
****************************************/

/***************************************
Static event handlers 
****************************************/

  firebase.auth().onAuthStateChanged(function(user) {
    if (user && user.uid == currentUid) {
      return;
    }
    user ? handleSignedInUser(user) : handleSignedOutUser();
  });

  $(window).on("popstate load", function(e) {
    var popstate = e.originalEvent.state;
    if(!popstate || popstate.view == "binders") {
      $("#return-to-binders-list").click();
    }
    else if(popstate.view == "detail") {
      history.pushState({view: "binders"}, "", '/index.html');
      $("#binders-list").find(".binder[data-id='" + popstate.id + "'] .cover").click();
    }
  });

  // Logout Button
  $('#btn-logout').on('click', function() {
    $("#loggedin").fadeOut(400, function() {
      firebase.auth().signOut().then(function() {
        // If reload is not desired, make sure to reset all views.
        window.location.reload();
      });
    });
  });

  // Static event handlers
  // Close dialogs on window click
  $(window).on("click", function(event) {
    if($(event.target).hasClass("dialog-backdrop")) {
      $(".dialog-backdrop .cancel-dialog").click();
    }
  });

  // New Binder Dialog
  $("#new-binder").on("click", function() {
    var $dialog = $("#frm-add-binder");
    $dialog.find(".color-picker li:first-child").click();
    $dialog.find("button span.create").show();
    $dialog.find("button span.update").hide();
    $dialog.show();
  });

  // Select color in New Binder dialog
  $(".color-picker li").on("click", pickColor);

  // Add Binder Dialog
  $("#frm-add-binder #add-binder").on("click", function() {
    addOrUpdateBinder();
  });

  // New Binder Dismiss
  $("#frm-add-binder .cancel-dialog").on("click", function() {
    $dialog = $("#frm-add-binder");
    $form = $dialog.find("form");
    // Reset form
    $form.get(0).reset();
    // Reset color picker
    $form.find(".color-picker li").removeClass("material-icons").html("");
    $form.find("input[type='hidden']").each(function() {
      $(this).val('');
    }); 
    // Close dialog
    $dialog.hide();
  });

  // Confirm Delete Binder Dismiss Button
  $("#frm-confirm-delete-binder .cancel-dialog").on("click", function() {
    $dialog = $("#frm-confirm-delete-binder");
    $form = $dialog.find("form");
    // Reset hidden form fields
    $form.find("input[type='hidden']").each(function() {
      $(this).val('');
    }); 
    // Close dialog
    $dialog.hide();
  });

  // Confirm Delete Binder Accept Button
  $("#confirm-delete-binder").on("click", function() {
    deleteBinder($(".binder[data-id='" + $(this).closest("form").find("#delete-binder-id").val() + "']"));
  });

  // Return to Binders List
  $("#return-to-binders-list").on("click", function() {
    $("#binder-detail").hide();
    $("#loggedin nav").addClass("reclaim-color");
    $("#binders-list").show();
  });

  // New Link Dialog
  $("#new-link").on("click", function() {
    var $dialog = $("#frm-add-link");
    $dialog.find("button span.create").show();
    $dialog.find("button span.update").hide();
    $dialog.show();
  });

  // Add Link Dialog
  $("#frm-add-link #add-link").on("click", function() {
    addOrUpdateLink();
  });

  // New Link Dismiss
  $("#frm-add-link .cancel-dialog").on("click", function() {
    $dialog = $("#frm-add-link");
    $form = $dialog.find("form");
    // Reset form
    $form.get(0).reset();
    $form.find("input[type='hidden']").each(function() {
      $(this).val('');
    });
    // Close dialog
    $dialog.hide();
  });

  // Confirm Delete Link Dismiss Button
  $("#frm-confirm-delete-link .cancel-dialog").on("click", function() {
    $dialog = $("#frm-confirm-delete-link");
    $form = $dialog.find("form");
    // Reset hidden form fields
    $form.find("input[type='hidden']").each(function() {
      $(this).val('');
    }); 
    // Close dialog
    $dialog.hide();
  });

  // Confirm Delete Link Accept Button
  $("#confirm-delete-link").on("click", function() {
    deleteLink($(".link[data-id='" + $(this).closest("form").find("#delete-link-id").val() + "']"));
  });

  // Drag & Drop document URL inside a binder in Binder Detail View
  $("#binder-detail").on("dragenter dragleave dragover drop", function(e) {
    urlDragDrop(e.originalEvent, $("#input-link-url"), $(this));
  });

  // Drag & Drop cover image URL on the New Binder Dialog
  $("#frm-add-binder .dialog").on("dragenter dragleave dragover drop", function(e) {
    urlDragDrop(e.originalEvent, $("#input-binder-cover"), $(this).parent());
  });

  // Prevent Drag & Drop URL anywhere not registered with an explicit event handler
  $("body").on("dragenter dragleave dragover drop", function(e) {
    e.originalEvent.preventDefault();
    switch(e.originalEvent.type) {
      case "dragover":
      case "dragenter":
          e.originalEvent.returnValue = false;
          break;
      case "drop":
          e.originalEvent.returnValue = false;
          e.originalEvent.preventDefault();
    }
  });

  /*
    TODO : Drawer Menu
  */
  // var MDCTemporaryDrawer = mdc.drawer.MDCTemporaryDrawer;
  // var drawer = new MDCTemporaryDrawer(document.querySelector('.mdc-temporary-drawer'));
  // document.querySelector('.prefs-menu').addEventListener('click', function() {
  //   drawer.open = true;
  // });

  // TODO : remove
  // $("#dropzone").on("mouseout", function(e) {
  //   console.log("Dropzone mouseout");
  //   $(this).hide();
  // });

/***************************************
END - Static event handlers 
****************************************/

/***************************************
Delegated event handlers 
****************************************/

  // Drag & Drop document URL directly on a binder in Binders List View
  $("#binders-list").on("dragenter dragleave dragover drop", ".binder", function(e) {
    urlDragDrop(e.originalEvent, $("#input-link-url"), $(this));
  });

  // Prevent images inside the page from being dragged.
  $("#loggedin").on("dragstart", "img, a[href], p.url span", function(e) {
    e.originalEvent.preventDefault();
    switch(e.originalEvent.type) {
      case "dragstart":
          e.originalEvent.returnValue = false;
          break;
      // case "drop":
      //     e.originalEvent.returnValue = false;
      //     e.originalEvent.preventDefault();
    }
  });

  // Handle open/close menu components
  $("#loggedin").on("click", ".menu-trigger", function() {
    var menu = new mdc.menu.MDCSimpleMenu($(this).parent().find('.mdc-simple-menu').get(0));
    menu.open = !menu.open;
  });

  // Show/Hide Binder action menu
  $("#binders-list").on("click", ".mdc-card", function() {
    $(this).find($(".mdc-card__actions")).show();
  });

  // Open a Binder
  $("#binders-list").on("click", ".binder .info, .binder .cover", function() {
    var $this = $(this);
    var $binder = $this.closest(".binder");
    var $binderColor = $binder.attr('data-color');
    var $binderCoverImage = $binder.find('.cover img').attr('src');
    $("#binders-list").hide();
    // Only for Demo
    // TODO : Remove b4 prod and replace by passing binder-id.
    $("#binder-detail header .info .title").html($this.parent().find(".title").html());
    $("#binder-detail header .info .updated").html($this.parent().find(".updated").html());
    //$("#binder-detail header").attr("data-color",$binderColor);
    $("#binder-detail header").attr("data-id",$binder.attr('data-id'));
    $("#binder-detail header .cover").attr("data-color",$binderColor);
    if(typeof $binderCoverImage != "undefined") {
      $("#binder-detail header .cover").removeClass("cover-border-width");
      $("#binder-detail header .cover .inner-cover").css("background-image","url(" + $binderCoverImage + ")");
    }
    else {
      $("#binder-detail header .cover").addClass("cover-border-width");
      $("#binder-detail header .cover .inner-cover").css("background-image","none");
    }
    // // Change topbar and FAB bgcolor
    // $("#loggedin nav").removeClass("reclaim-color");
    // var topbarColor = (($binderColor == "FFFFFF")
    //                     ?shadeColor("#"+$binderColor, -0.25)
    //                     :shadeColor("#"+$binderColor, -0.10));
    // $("#loggedin nav").css("background-color", topbarColor);
    // $("#new-link").css("background-color", topbarColor);
    
    displayBinderContent();
    // Scroll the page to the top.
    window.scroll(0, 1);
    var binderID = $this.closest(".binder").attr("data-id");
    history.pushState({view: "detail", id: binderID}, "", '/' + binderID);
  });

  // Show Confirm Delete Binder dialog
  $("#binders-list").on("click", ".binder .delete-binder", function() {
    $binder = $(this).closest(".binder");
    $frm = $("#frm-confirm-delete-binder");
    $frm.find("#delete-binder-id").val($binder.attr("data-id"));
    $frm.find(".binder-name").html($binder.find(".info .title").html());
    $frm.show();
  });

  // Show Confirm Delete Link dialog
  $("#binder-detail").on("click", ".link .delete-link", function() {
    $link = $(this).closest(".link");
    $frm = $("#frm-confirm-delete-link");
    $frm.find("#delete-link-id").val($link.attr("data-id"));
    $frm.find(".link-name").html($link.find(".info .title a").html());
    $frm.show();
  });

  // $("#binder-detail").on("click", ".link .delete-link", function() {
  //   deleteLink($(this).closest(".link"));
  // });

  $("#binders-list").on("click", ".binder .edit-binder", function() {
    prepareEditBinder($(this).closest(".binder"));
  });

  $("#binder-detail").on("click", ".link .edit-link", function() {
    prepareEditLink($(this).closest(".link"));
  });

/***************************************
END - Delegated event handlers 
****************************************/

/***************************************
Initialization code
****************************************/
  // Initialize all Material Design Components effects.
  mdc.autoInit();
  
  // Handle Binder Sorting
  if(!iOS) {
    Sortable.create(document.querySelector("#binders-list"), {
      animation: 350,
      //filter: ".sortable-exclude",
      handle: ".handle",
      onMove: function (evt) {
        return evt.related.className.indexOf('sortable-exclude') === -1;
      },
      onUpdate: function (evt) {
        sortedBinders = [];
        $("#binders-list").find(".mdc-card[data-id]").each(function() {
          sortedBinders.push($(this).attr("data-id"));
        });
        saveSortedBinders();
      }
    });
  }

  // Handle Documents Sorting
  if(!iOS) {
    Sortable.create(document.querySelector("#links-list"), {
      draggable: "article",
      animation: 350,
      handle: ".handle",
      onUpdate: function (evt) {
        sortedLinks = [];
        $("#links-list").find(".link[data-id]").each(function() {
          sortedLinks.push($(this).attr("data-id"));
        });
        saveSortedLinks();
      }
    });
  }

/***************************************
END - Initialization code
****************************************/

/**************************************
Main methods
***************************************/
  /**
   * Sign in user with Firebase authentication
   * @param  {Object} user Firebase auth User object
   * @return {void} 
   */
  function handleSignedInUser(user) {
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = (user.photoURL)?user.photoURL:"assets/img/default-avatar.jpg";
    var uid = user.uid;
    var providerData = user.providerData;

    currentUid = user.uid;
    $(".view").hide();
    $("#loggedin").show();
    $("#binders-list").css('display', 'flex');
    
    user.getToken().then(function(accessToken) {
      //console.log("Access Token : " + accessToken);
    });
    
    $("#user-full-name span").html(displayName);
    $("#user-full-name img").attr('src', photoURL).attr('alt', displayName);

    // Set storage path for this user.
    // Storage tested and working but abandonned on current App iteration.
    // fbuStore = fbStorage.ref().child('user/' + currentUid + '/coverImages/');
    
    // Set Database ref for this user.
    fbuDbRoot = fbDatabase.ref().child('user/' + currentUid);
    fbuBinders = fbDatabase.ref().child('user/' + currentUid + '/binders/');
    displayBinders();
  }

  /**
   * Signout User using Firebase Auth
   * @return {[type]} [description]
   */
  function handleSignedOutUser() {
    // Show login page
    $("#loggedout").show();
    // The start method will wait until the DOM is loaded.
    ui.start('#firebaseui-auth-container', uiConfig);
  };

  /**
   * Saves binders sort order array in DB
   * @return {void}
   */
  function saveSortedBinders() {
    fbuDbRoot.child("binder-sort").set(sortedBinders);
  }

  /**
   * Saves links sort order array in DB
   * @return {void}
   */
  function saveSortedLinks() {
    fbuCurrentBinder.child("link-sort").set(sortedLinks);
  }

  /**
   * Handles all Drag & Drop events
   * @param  {Event} evt      DOM Event
   * @param  {Object} $frmElt  The jQuery Object containing the targetted form element
   * @param  {Object} $context jQuery Object containing  the element where the drop is made
   * @return {Boolean} 
   */
  function urlDragDrop(evt, $frmElt, $context) {
    var $dropzone = $("#dropzone");
    var $binder = $context.hasClass("binder")?$context:false;
    var $newBinderDialog = ($context.attr("id")=="frm-add-binder")?$context:false;
    var $binderCoverInput = false;
    var dt = evt.dataTransfer;
    var objIsUrl = false;
    if (dt.types && dt.types.length > 1 && (dt.types[0].match(/url|uri/i) || dt.types[1].match(/url|uri/i))) {
      objIsUrl = true;
    }
    if($newBinderDialog) {
      $dropzone.find(".cover-url-msg").show();
      $dropzone.find(".doc-url-msg").hide();
      $binderCoverInput = $newBinderDialog.find("#input-binder-cover");
    }
    else {
      $dropzone.find(".cover-url-msg").hide();
      $dropzone.find(".doc-url-msg").show(); 
      var binderTitle = $context.find("p.title").html();
      $("#dropzone .target-binder-name").html(binderTitle);
    }
    if(objIsUrl) {
      switch(evt.type) {
        case "dragover":
          $dropzone.show();
          evt.preventDefault();
        case "dragenter":
          if($binder) {
            $binder.addClass("highlighted");
          }
          if($newBinderDialog) {
            $binderCoverInput.addClass("highlighted");
          }
          evt.returnValue = false;
          break;
        case "dragleave":
          $dropzone.hide();
          if($binder) {
            $binder.removeClass("highlighted");
          }
          if($newBinderDialog) {
            $binderCoverInput.removeClass("highlighted");
          }
          break;
        case "drop":
          $dropzone.hide();
          var linkUrl = evt.dataTransfer.getData("text/plain");
          if($binder) {
            $binder.find(".info").click();
            $binder.removeClass("highlighted");
          }
          $frmElt.val(linkUrl);
          if($newBinderDialog) {
            $binderCoverInput.removeClass("highlighted");
          }
          else {
            // Get the link title.
            $.ajax({
                url: phpServerAddress + "binder3-functions.php?func=getDocTitle",
                method: "POST",
                dataType: "text",
                data: $("#frm-add-link form").serialize(),
                success: function(response) {
                  $("#input-link-title").val(response);
              }});
          }
          $frmElt.closest("aside").show();
          evt.preventDefault();
          evt.stopPropagation();
          evt.returnValue = false;
          return false;
      }
    }
  }

  /**
   * Adds/Update a binder in DB and refresh Binders List
   */
  function addOrUpdateBinder() {
    var $form = $("#frm-add-binder form");
    var $binderNameElt = $form.find("#input-binder-name");
    var $binderColorElt = $form.find("#input-binder-color");
    // If binder name is valid
    if($binderNameElt.get(0).checkValidity()) {
      // Used to distinguish new/update
      var binderID = $form.find("#input-binder-id").val();
      var binderCoverUrl = $form.find("#input-binder-cover").val();
      // Used to distinguish new/update
      var binderCoverUrlOld = $form.find("#input-binder-cover-old").val();
      // Firebase DB refs
      var binderRef = null;
      var coverRef = null;
      // Object that will be added or updated in the DB
      var binderNewValues = {
        name: $binderNameElt.val(),
        color: $binderColorElt.val(),
        coverURL: binderCoverUrl,
        updated: firebase.database.ServerValue.TIMESTAMP
      }
      // Init sort array if it is null.
      if(!sortedBinders) {
        sortedBinders = [];
      }
      // If no binder ID, add new binder ...
      if(binderID == "") {
        // Create a new ref and set its value.
        binderRef = fbuBinders.push();
        binderRef.set(binderNewValues);
        // Add this binder's ID to the beginning of the sort array
        sortedBinders.unshift(binderRef.key);
        saveSortedBinders();
      }
      // ... else, update existing binder
      else {
        // Find binder ref from its ID and update its value
        binderRef = fbuBinders.child(binderID);
        binderRef.update(binderNewValues);
      }
      coverRef = binderRef.child("cover");
      // If we have a new cover (new or update) ...
      if(binderCoverUrl != binderCoverUrlOld) {
        // Resize Image cover and store it in the same binder.
        //$.support.cors = true;
        $.ajax({
          url: phpServerAddress + "binder3-functions.php?func=resizeImage",
          method: "POST",
          dataType: "json",
          data: $form.serialize(),
          success: function(response) {
            if(response) {
              var imgData = response.imgData;
              // No need to use Storage for small images
              // var imgType = response.imgType.replace(/[^\/]*\//,'');
              //fbuStore.child(bid.key + "." + imgType).putString(imgData, 'data_url');
              coverRef.set(imgData);
            }
          }
        });
      }
      // Refresh Binders List
      displayBinders();
      // Sets a value change listener on the cover ref value to make sure
      // the cover image is refreshed when the image data is finally written to DB
      coverRef.on('value', function(snapshot) {
        if(snapshot.exists()) {
          refreshBinderCover(binderRef.key, snapshot.val());
          // Cancel event listener once we have our image refreshed.
          //coverRef.off();
        }
      });
      // Reset and close the dialog
      $("#frm-add-binder .cancel-dialog").click();
    }
  }

  /**
   * Check color in Color Picker of New/Update BInder dialog
   * @return {void}
   */
  function pickColor() {
    $(".color-picker li").removeClass("material-icons").html("");
    $(this).addClass("material-icons").html("done");
    $("#input-binder-color").val($(this).data("color"));
  }

  /**
   * Add/Update a document link
   */
  function addOrUpdateLink() {
    var $form = $("#frm-add-link form");
    var $linkTitleElt = $form.find("#input-link-title");
    var $linkURLElt = $form.find("#input-link-url");
    // Check form elements are valid (title and url)
    if($linkTitleElt.get(0).checkValidity() && $linkURLElt.get(0).checkValidity()) {
      var docRef = null;
      // Used for Update ops
      var docID = $form.find("#input-link-id").val();
      var docTitle = $linkTitleElt.val();
      var docURL = $linkURLElt.val();
      // Parse title and url to determine type of document
      var docType = setDocType(docTitle, docURL);

      // Values of link document to add/update
      var newDocValues = {
        title: docTitle,
        url: docURL,
        type: docType,
        updated: firebase.database.ServerValue.TIMESTAMP
      }

      if(!sortedLinks) {
        sortedLinks = [];
      }
      // If there is no ID, this is a new link...
      if(docID == "") {
        // Create new doc ref and set its value.
        docRef = fbuCurrentBinder.child("docs").push();
        docRef.set(newDocValues);
        // Add this Link's ID to the beginning of the sort array
        sortedLinks.unshift(docRef.key);
        saveSortedLinks();
      }
      // ... else, the document with this ID exists
      else {
        // Find the document and update its value.
        docRef = fbuCurrentBinder.child("docs/" + docID);
        docRef.update(newDocValues);
      }
      // When a link is added/updated, we also update the containing binder "updated" property
      fbuCurrentBinder.update({updated: firebase.database.ServerValue.TIMESTAMP});
      // Refresh Binder Detail view
      displayBinderContent();
      // Reset and close the dialog.
      $("#frm-add-link .cancel-dialog").click();
    }
  }

  /**
   * Get the binders list from DB and generate Binders List view in the DOM
   * @return {void}
   */
  function displayBinders() {
    // Start by cleaning the DOM from current Binders List
    $("#binders-list article.binder:not(.template)").remove();
    // Get the binders list ref from DB
    fbuBinders.once('value').then(function(bindersSnapshot) {
      // If the list is empty ...
      if(!bindersSnapshot.exists()) {
        // Show the no-binder view
        $("#no-binder").addClass("show");
      }
      // ... else, create and display DOM for binders.
      else {
        var binders = bindersSnapshot.val();
        // Get sort order from DB
        fbuDbRoot.child("binder-sort").once("value").then(function(sortSnapshot) {
          // TODO : see if we can remove following logic at release.
          // Following conditional is required only because we have old data withour sort order array.
          // Check if sort array is stored in DB and if its consistant with binders list
          if(sortSnapshot.exists() && sortSnapshot.val().length == bindersSnapshot.numChildren()) {
            var savedSortOrder = sortSnapshot.val();
          }
          // ... else, create it from the binders' list 
          else {
            var savedSortOrder = [];
            bindersSnapshot.forEach(function(binder) {
              savedSortOrder.push(binder.key);
            });
          }
          // Hide the no-binder view
          $("#no-binder").removeClass("show"); 
          // Initialize sortedBinders array;
          sortedBinders = [];
          // Iterate through the binders list
          for(var i=0; i < savedSortOrder.length; i++) {
            // Binder's auto-generated ID.
            var key = savedSortOrder[i];
            // Binder details
            var info = binders[key];
            // Clone and set binder element in the DOM.
            var $binderTpl = $("article.binder.template").clone().removeClass("template");
            $binderTpl.attr("data-color", info.color);
            $binderTpl.css("background-color", '#' + info.color);
            $binderTpl.attr("data-id", key);
            // If we have a cover, use it ...
            if(info.cover) {
              $binderTpl.find(".cover").html("<img alt='' src='" + info.cover + "'>");
            }
            // ... else, use a lightened version of the binder color.
            else {
              $binderTpl.find(".cover").addClass("no-image").css("background-color", shadeColor("#"+info.color, 0.3));
            }
            $binderTpl.find(".info .title").html(info.name);
            $binderTpl.find(".info .updated span").html(formatDate(info.updated));
            // Append this binder
            $("#binders-list").append($binderTpl);
            // Update sort array
            sortedBinders.push(key);
          }
          // Save sort array to DB
          saveSortedBinders();
        });
      }
    });
    
  }

  /**
   * Refreshes the binder cover once it has been resized and saved in the DB
   * @param  {String} binderID Binder ID in the DB
   * @param  {String} cover    Data-URI String of the cover image
   * @return {void}
   */
  function refreshBinderCover(binderID, cover) {
    $(".binder[data-id='" + binderID + "']").find(".cover").removeClass("no-image").html("<img alt='' src='" + cover + "'>");
  }

  /**
   * Deletes binder from the DB and the DOM
   * @param  {Object} binderElt jQuery object containing the DOM element corresponding to a binder.
   * @return {void}
   */
  function deleteBinder(binderElt) {
    var binderID = binderElt.attr("data-id");
    // Remove ref from DB
    fbuBinders.child(binderID).remove();
    // Remove DOM element
    binderElt.remove();
    // Remove this new binder's ID from the sortedBinders array
    sortedBinders.splice(sortedBinders.indexOf(binderID), 1);
    saveSortedBinders();
    // Show/Hide no-binder view
    if($("#binders-list .binder:not(.template)").length == 0) {
      $("#no-binder").addClass("show");
    }
    else {
      $("#no-binder").removeClass("show"); 
    }
    // Reset and hide the remove binder confirmation dialog.
    $("#frm-confirm-delete-binder .cancel-dialog").click();
  }

  /**
   * Fills and shows Add/Update Binder form
   * @param  {Object} binderElt jQuery object containing a binder DOM element.
   * @return {[type]}           [description]
   */
  function prepareEditBinder(binderElt) {
    var $dialog = $("#frm-add-binder");
    // Find this binder's ref in the DB
    var binderRef = fbuBinders.child(binderElt.attr("data-id"));
    // Fill the form with the values from the DB
    binderRef.once('value').then(function(snapshot) {
      var binderVals = snapshot.val();
      $dialog.find("#input-binder-id").val(snapshot.key);
      $dialog.find("#input-binder-name").val(binderVals.name);
      if(binderVals.coverURL) {
        $dialog.find("#input-binder-cover").val(binderVals.coverURL);
        // This hidden form element will be used to compare cover URL to detect new cover changes.
        $dialog.find("#input-binder-cover-old").val(binderVals.coverURL); 
      }
      // Select the binder color if set
      if(binderVals.color) {
        $dialog.find("#input-binder-color").val(binderVals.color); 
        $dialog.find(".color-picker li[data-color='" + binderVals.color + "']").click();
      }
    });
    // Update label of the Add/Update button
    $dialog.find("button span.create").hide();
    $dialog.find("button span.update").show();
    // Show Add/Update binder dialog
    $dialog.show();
  }

  /**
   * Get a binder detail from DB and generate Binder Detail view in the DOM
   * @return {void}
   */
  function displayBinderContent() {
    // Re-initialize the DOM, removing any BInder Detail.
    $("#links-list article.link:not(.template)").remove();
    // Set current binder ref
    fbuCurrentBinder = fbuBinders.child($("#binder-detail #binder-detail-header").attr("data-id"));
    // Gets the docs ref
    var docs = fbuCurrentBinder.child("docs");
    // Gets the content of the docs ref
    docs.once('value').then(function(linksSnapshot) {
      // If no content, show corresponding message.
      if(!linksSnapshot.exists()) {
        $("#no-docs").addClass("show");
      }
      // ... else, generate DOM and display content.
      else {
        var links = linksSnapshot.val();
        // Get sort order from DB
        fbuCurrentBinder.child("link-sort").once("value").then(function(sortSnapshot) {
          // Check if sort array is stored in DB and if its consistant with links list
          if(sortSnapshot.exists() && sortSnapshot.val().length == linksSnapshot.numChildren()) {
            var savedSortOrder = sortSnapshot.val();
          }
          // ... else, create it from the links' list 
          else {
            var savedSortOrder = [];
            linksSnapshot.forEach(function(link) {
              savedSortOrder.push(link.key);
            });
          }
          $("#no-docs").removeClass("show"); 

          // Reset local sortedLinks array
          sortedLinks = [];
          // For each link in the docs ...
          for(var i=0; i < savedSortOrder.length; i++) {
            // Link's auto-generated ID.
            var key = savedSortOrder[i];
            // Link details
            var info = links[key];
            var $linkTpl = $("article.link.template").clone().removeClass("template");
            $linkTpl.attr("data-id", key);
            $linkTpl.find(".type img").attr("src", $linkTpl.find(".type img").attr("src").replace(/unknown/i,info.type));
            $linkTpl.find(".info .title a").attr("href", info.url);
            $linkTpl.find(".info .title a").html(info.title);
            $linkTpl.find(".info .file .date").html(formatDate(info.updated));
            $linkTpl.find(".info .url span").html(info.url);
            // Add the new link to the beginning of the list in the DOM.
            $("#links-list").append($linkTpl);
            // Update sort array
            sortedLinks.push(key);
          }
          // Save sort array to DB
          saveSortedLinks();
        });
      }
    });
    $("#binder-detail").show();
    
  }

  /**
   * Delete a link from the DB and the DOM
   * @param  {Object} linkElt jQuery object containg link DOM element
   * @return {void}
   */
  function deleteLink(linkElt) {
    var linkID = linkElt.attr("data-id");
    // Remove the DB ref
    fbuCurrentBinder.child("docs/" + linkID).remove();
    // Remove the DOM element
    linkElt.remove();
    // Remove this new link's ID from the sort array
    sortedLinks.splice(sortedLinks.indexOf(linkID), 1);
    saveSortedLinks();
    // Show/Hide no-links message.
    if($("#binder-detail .link:not(.template)").length == 0) {
      $("#no-docs").addClass("show");
    }
    else {
      $("#no-docs").removeClass("show"); 
    }
    // Reset and hide the remove link confirmation dialog.
    $("#frm-confirm-delete-link .cancel-dialog").click();
  }

  /**
   * Fill and show Update Link dialog
   * @param  {Object} linkElt jQuery object containing Link DOM element
   * @return {void}
   */
  function prepareEditLink(linkElt) {
    var $dialog = $("#frm-add-link");
    var linkRef = fbuCurrentBinder.child('docs/' + linkElt.attr("data-id"));
    linkRef.once('value').then(function(snapshot) {
      var linkVals = snapshot.val();
      $dialog.find("#input-link-id").val(snapshot.key);
      $dialog.find("#input-link-title").val(linkVals.title);
      $dialog.find("#input-link-url").val(linkVals.url);
    });
    $dialog.find("button span.create").hide();
    $dialog.find("button span.update").show();
    $dialog.show();
  }

/**************************************
END - Main methods
***************************************/

/**************************************
Utility functions
***************************************/
  
  /**
   * Format a Timestamp to a Date String
   * @param  {Int} ts Timestamp
   * @return {String} Date in the format "November 5, 2025"
   */
  function formatDate(ts) {
    var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    var date = new Date(ts);
    return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
  }

  /**
   * Lighten a color
   * @param  {String} color   HEX RGB color
   * @param  {Float} percent Number in [-1,1] representing percent of color lightening
   * @return {String}         HEX RGB color
   */
  function shadeColor(color, percent) {   
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
  }

  /**
   * Sets a document link type according to custom logic
   * @param {String} title  Link Title
   * @param {String} urlStr Link URL
   */
  function setDocType(title, urlStr) {
    var type = "unknown";
    // Create a URL object from URL string
    var url = new URL(urlStr);
    var href = url.href;      // The whole URL string
    var host = url.hostname;  // The hostname (including domain name)
    var path = url.pathname;  // The part after the TLD and before the hash & params
    
    // Rules for Hostname patterns
    if(href.match(/sites\.google\.com\/corp\/google\.com/i)) {
      type = "google-website";
    }
    else if(host.match(/youtube\.com/i)) {
      type = "youtube";
    }
    else if(host.match(/datastudio\.google\.com/i)) {
      type = "google-data-studio";
    }
    else if(href.match(/(google\.com\/ddm\/|google\.com\/doubleclick\/)/i) 
        || host.match(/(ddm\.google\.com|doubleclick-advertisers\.googleblog\.com)/i)
        || host.match(/doubleclickbygoogle\.com/i)) {
      type = "doubleclick";
    }
    else if(host.match(/(google\.com|exceedlms\.com|googleplex\.com|richmediagallery\.com)/i)) {
      type = "google-corp";
    }
    else if(host.match(/theglobeandmail\.com/i)) {
      type = "globeandmail";
    }
    else if(host.match(/recode\.net/i)) {
      type = "recode";
    }
    else if(host.match(/ricardocuisine\.com/i)) {
      type = "ricardo";
    }
    else if(host.match(/ted\.com/i)) {
      type = "ted";
    }
    else if(host.match(/trainingpeaks\.com/i)) {
      type = "trainingpeaks";
    }
    else if(host.match(/trainright\.com/i)) {
      type = "trainright";
    }
    else if(host.match(/underarmour\.com/i)) {
      type = "underarmour";
    }
    else if(host.match(/linkedin\.com/i)) {
      type = "linkedin";
    }
    else if(host.match(/facebook\.com/i)) {
      type = "facebook";
    }
    else if(host.match(/twitter\.com/i)) {
      type = "twitter";
    }
    else if(host.match(/lapresse\.ca/i)) {
      type = "lapresse";
    }
    else if(host.match(/nytimes\.com/i)) {
      type = "nytimes";
    }
    else if(host.match(/cbc\.ca/i) || host.match(/radio-canada\.ca/i)) {
      type = "cbc";
    }

    // Rules for Title and href patterns (supersedes previous rules)
    if(title.match(/Google Docs/i) 
        || path.match(/\.(doc|docx)/i)
        || href.match(/google\.com\/document/i)
        ) {
      type = "word";
    }
    else if(title.match(/Google Sheets/i) 
              || path.match(/\.(xls|xlsx)/i)
              || href.match(/google\.com\/spreadsheets/i)
        ) {
      type = "sheet";
    }
    else if(title.match(/Google Slides/i) 
              || path.match(/\.(ppt|pptx)/i)
              || href.match(/google\.com\/presentation/i)
        ) {
      type = "slide";
    }
    else if(title.match(/Google Forms/i) 
              || href.match(/google\.com\/forms/i)
        ) {
      type = "form";
    }
    
    // else if(title.match(/\.(pdf)/i) || url.match(/\.(pdf)/i)) {
    //   type = "pdf";
    // }  
    return type;
  }

/**************************************
END - Utility functions
***************************************/
})();